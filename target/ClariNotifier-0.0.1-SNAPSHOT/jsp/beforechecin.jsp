<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@page import="com.healspace.clari.notifier.PatientInfo"%>
<!DOCTYPE html>
<!-- Sh G N -->
<html lang="en-US" prefix="og:http://ogp.me/ns#" xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml" itemscope itemtype="http://schema.org/Article">

<head>
<c:set var="contextPath" scope="session" value=""/>
  
<meta charset="UTF-8">
<link rel="dns-prefetch" href="//fonts.googleapis.com">
<link rel="dns-prefetch" href="//fonts.gstatic.com">
<link rel="dns-prefetch" href="//platform.twitter.com">
<link rel="dns-prefetch" href="//pbs.twimg.com">

<title><c:out value="${page_title}" escapeXml="false"/></title>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<meta http-equiv="content-language" content="<c:out value="${page_language}" escapeXml="false"/>">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">

<meta name="viewport" content="width=device-width, initial-scale=1">
	
<link href="https://plus.google.com/+Rujhaan/" rel="publisher">

<link rel="stylesheet" href="${contextPath}/static/css/bootstrap.css" media="screen">
<link href="/ipin/css/bootstrap.css" rel="stylesheet">
<link href="/ipin/css/font-awesome.css" rel="stylesheet">
<link href="/ipin/style.css?ver=103" rel="stylesheet">
<link rel="stylesheet" href="${contextPath}/static/css/style.css?ver=103">

<style>

.navbar .brand {
	float: left;
	display: block;
    padding: 10px 5px 1px 15px;/*10px 20px 1px;*/
	margin-left: -20px;
	font-size: 20px;
	font-weight: 200;
	color: #777777;
	text-shadow: 0 1px 0 #ffffff;
}

img.logo {
margin: 0px;
}

@media all and (max-width: 767px){
body {
padding-left: 2px;
padding-right: 2px;
}
}

@media all and (max-width: 767px){
.navbar-fixed-top, .navbar-fixed-bottom, .navbar-static-top {
margin-left: 1px;
margin-right: 1px;
}
}

@media all and (max-width: 767px){
.container {
margin-right: auto;
margin-left: auto;
padding-left: 1px;
padding-right: 15px;
}
}

.header_pane {
font-size: 18px;/*20px;*/
font-weight: bold;
height: 80px;
padding: 15px 5px 25px 1px;
color: #111;/*#6F5A5A;*/
}

@media all and (max-width: 979px){
.navbar-fixed-top {
  margin-bottom: 10px;
}
}

@media all and (min-width: 979px) {
	.navbar-fixed-top {
		margin-bottom: 20px;
	}
}

img.twitter-icon {
	width: 38px;
	height: 26px;
	/* padding: 4px 1px; */
}

i.icon-twitter.icon-large-color {
	font-size: 1.3333333333333333em;
	color: #45b0e3;
	margin: 1px 1px 1px 3px;
}

.page-header {
	padding-bottom: 9.5px;
	margin: 5px 0 1px;
	border-bottom: 1px solid transparent;
}

.navbar .nav>li>a {
	float: none;
	padding: 10px 15px 10px;
	color: #777777;
	text-decoration: none;
	text-shadow: 0 1px 0 #ffffff;
	color: #111; /*#ff1200;*/
	text-transform: uppercase;
	font-size: 13.5px;
}

.menu-bar {
/* height: 32px; */
/* width: 954px; */
/* border: 2px solid #f0f0f0; */
border-top: 0;
border-radius: 0 0 2px 2px;
padding: 0 10px;
font-size: 12px;
background: #fff;
}

.menu-bar li {
line-height: 20px;
/* border-right: 1px solid #ccc; */
/* margin-right: 1px; */
/* padding-right: 1px; */
/* margin-top: 1px; */
/* margin-bottom: 5px; */
/* float: left; */
/* padding: 5px 0; */
/* display: block; */
/* color: #1769ff; */
/* text-align: center; */
/* font-size: 14px; */
/* margin-left: 0px; */
/* padding-left: 0px; */
list-style: none;
display: inline-block;
}
.menu-bar ul li a{
display: block;
padding: 0 6px;
margin: 10px 0;
border-right: 1px solid #e8e8e8;
font-size: 14px;
text-transform: uppercase;
color: rgb(33, 33, 33);
}
</style>

<script type="text/javascript" src="${contextPath}/static/js/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="${contextPath}/static/js/snowstorm.js"></script>
  </head>
<body>
	<div class="container">
		<div class="page-header" id="banner">
		</div>

		<!-- Containers
      ================================================== -->
		<div class="bs-docs-section">
			<div class="row">
				<div id="galleryfeed-container" class="col-lg-4">
					<div class="bs-component">
 					<form action="CheckinServlet?action=checkin" method="post">
 					<table>
 					<tr><label>Patient Name</label><input type="text" name="patientName"></input></tr>
 					<tr></table><label>Mobile No.</label><input type="text" name="mobileNumber"></input></tr>
					  <tr><button>Submit</button></tr>>
					</form>
					</table>
					<c:out value="${message}"></c:out>
					
					</div>
				</div>
			</div>
		</div>
	</div>

   <input type="hidden" id="country_val" name="country_val" value="<c:out value="${country}" escapeXml="false"/>"/>
   <input type="hidden" id="cs_val" name="cs_val" value="<c:out value="${cs}" escapeXml="false"/>"/>
   <input type="hidden" id="category" name="category" value="<c:out value="${category}" escapeXml="false"/>"/>
   
  </body>
</html>
