package com.healspace.clari.notifier;

import java.util.ArrayList;
import java.util.List;

public class MeanSDWaitingTimeCalcTest {

	public static void main(String[] args) {
		WaitingTimeCalc calc = new MeanSDWaitingTimeCalc();
		
		WaitingTimeCalc calc1 = new Mean2SDWaitingTimeCalc();

		List<PatientInfo> donePatients = new ArrayList<PatientInfo>();

		PatientInfo pi = new PatientInfo();
		pi.timeTakenInConsultaton = 120;
		donePatients.add(pi);

		PatientInfo pi1 = new PatientInfo();
		pi1.timeTakenInConsultaton = 90;
		donePatients.add(pi1);

		PatientInfo pi2 = new PatientInfo();
		pi2.timeTakenInConsultaton = 150;
		donePatients.add(pi2);

		PatientInfo pi3 = new PatientInfo();
		pi3.timeTakenInConsultaton = 70;
		donePatients.add(pi3);

		PatientInfo pi4 = new PatientInfo();
		pi4.timeTakenInConsultaton = 110;
		donePatients.add(pi4);

		PatientInfo pi6 = new PatientInfo();
		pi6.timeTakenInConsultaton = 120;
		donePatients.add(pi6);

		double[] vals = calc.calculateConsultationTime(donePatients);
		System.out.println("vals:" + vals[0] + "--" + vals[1]+ "--" + vals[2]);
		
		double[] vals1 = calc1.calculateConsultationTime(donePatients);
		System.out.println("vals:" + vals1[0] + "--" + vals1[1]+ "--" + vals[2]);
	}
}
