package com.healspace.clari.notifier;

import java.util.List;

public interface WaitingTimeCalc {

	double[] calculateConsultationTime(List<PatientInfo> patientsInfo);

}
