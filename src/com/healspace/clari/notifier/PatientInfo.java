package com.healspace.clari.notifier;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Indexed;

@Entity("patients")
public class PatientInfo implements Serializable {

	public static AtomicInteger currentPatientNumber = new AtomicInteger(0);
	public static AtomicInteger currentTokenNumber = new AtomicInteger(0);

	@Id
	public ObjectId id;
	
	@Indexed
	public int patientNumber;

	@Indexed()
	//	@Indexed(unique = true)
	public String patientName;

	@Indexed
	public String mobileNumber;
	
	@Indexed
	public long receptionEntryTime;
	
	@Indexed
	public long doctorCallTime;
	
	@Indexed
	public long timeTakenInConsultaton;
	
	@Indexed
	// doctorCallTime - receptionEntryTime
	public long totalWaitingTime;
	
	@Indexed
	public int tokenNumber;
	
	@Indexed
	public long expectedWaitingTime;
	
	@Indexed
	public long expected2ndWaitingTime;
	
	@Indexed
	public boolean consultationDone;
	
	@Indexed
	public boolean smsSentToPatitent;
	
	@Indexed
	public long smsSentTime;


	public PatientInfo() {
		patientNumber = currentPatientNumber.incrementAndGet();
		tokenNumber = currentTokenNumber.incrementAndGet();
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public long getReceptionEntryTime() {
		return receptionEntryTime;
	}

	public void setReceptionEntryTime(long receptionEntryTime) {
		this.receptionEntryTime = receptionEntryTime;
	}

	public long getDoctorCallTime() {
		return doctorCallTime;
	}

	public void setDoctorCallTime(long doctorCallTime) {
		this.doctorCallTime = doctorCallTime;
	}

	public static AtomicInteger getCurrentPatientNumber() {
		return currentPatientNumber;
	}

	public static void setCurrentPatientNumber(
			AtomicInteger currentPatientNumber) {
		PatientInfo.currentPatientNumber = currentPatientNumber;
	}

	public static AtomicInteger getCurrentTokenNumber() {
		return currentTokenNumber;
	}

	public static void setCurrentTokenNumber(AtomicInteger currentTokenNumber) {
		PatientInfo.currentTokenNumber = currentTokenNumber;
	}

	public long getTimeTakenInConsultaton() {
		return timeTakenInConsultaton;
	}

	public void setTimeTakenInConsultaton(long timeTakenInConsultaton) {
		this.timeTakenInConsultaton = timeTakenInConsultaton;
	}

	public int getPatientNumber() {
		return patientNumber;
	}

	public void setPatientNumber(int patientNumber) {
		this.patientNumber = patientNumber;
	}

	public int getTokenNumber() {
		return tokenNumber;
	}

	public void setTokenNumber(int tokenNumber) {
		this.tokenNumber = tokenNumber;
	}
	
	public long getTotalWaitingTime() {
		return totalWaitingTime;
	}

	public void setTotalWaitingTime(long totalWaitingTime) {
		this.totalWaitingTime = totalWaitingTime;
	}
	
	public long getExpectedWaitingTime() {
		return expectedWaitingTime;
	}

	public void setExpectedWaitingTime(long expectedWaitingTime) {
		this.expectedWaitingTime = expectedWaitingTime;
	}
	
	public long getExpected2ndWaitingTime() {
		return expected2ndWaitingTime;
	}

	public void setExpected2ndWaitingTime(long expected2ndWaitingTime) {
		this.expected2ndWaitingTime = expected2ndWaitingTime;
	}
	
	public boolean isConsultationDone() {
		return consultationDone;
	}

	public void setConsultationDone(boolean consultationDone) {
		this.consultationDone = consultationDone;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public long avgConsultationTime;


	String message = "Hello, %s, your turn is about to come in next %d mins.";

	@Override
	public String toString() {
		return "PatientInfo [id=" + id + ", patientNumber=" + patientNumber
				+ ", patientName=" + patientName + ", mobileNumber="
				+ mobileNumber + ", receptionEntryTime=" + receptionEntryTime
				+ ", doctorCallTime=" + doctorCallTime
				+ ", timeTakenInConsultaton=" + timeTakenInConsultaton
				+ ", totalWaitingTime=" + totalWaitingTime + ", tokenNumber="
				+ tokenNumber + ", expectedWaitingTime=" + expectedWaitingTime
				+ ", expected2ndWaitingTime=" + expected2ndWaitingTime
				+ ", consultationDone=" + consultationDone + ", message="
				+ message + "]";
	}

	public String getExpectedWatingTimeMessage() {
		long waitingTimeInMins = TimeUnit.SECONDS.toMinutes(expectedWaitingTime);
		String notifymessage = String.format(message, this.patientName,
				waitingTimeInMins);
		return notifymessage;
	}

}
