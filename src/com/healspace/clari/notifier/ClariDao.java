package com.healspace.clari.notifier;

import java.util.List;

import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.query.UpdateOperations;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

public class ClariDao extends MongoDao {

  public static String COLLECION = "patients";

  public Datastore ds;

  public ClariDao() {
    super("127.0.0.1", COLLECION);
    Morphia morphia = new Morphia();
    ds = morphia.createDatastore(mongoClient, "clari");
    morphia.map(PatientInfo.class);
    // ensureIndex(Class<T> clazz, String name, String fields, boolean unique,
    // boolean dropDupsOnCreate)
    // Ensures (creating if necessary) the index including the field(s) +
    // directions; eg fields = "field1, -field2" ({field1:1, field2:-1})
    //ds.getCollection(Email.class).ensureIndex(Email.class, EMAIL_COLLECION, "emailId", true, true);
    DBObject dbObject = new BasicDBObject("patientNumber", 1);
    ds.getCollection(PatientInfo.class).ensureIndex(dbObject, null, true);
  }

  public void add(List<PatientInfo> PatientInfos) {
    ds.save(PatientInfos);
  }

  public void add(PatientInfo PatientInfo) {
    ds.save(PatientInfo);
  }
  
  public PatientInfo find() {
    PatientInfo obj = ds.find(PatientInfo.class).get();
    return obj;
  }
  

//  public static void main(String[] args) {
//    ClariDao dao = new ClariDao();
//    PatientInfo e = new PatientInfo();
//    ObjectId id = new ObjectId();
//    e.id = id;
//    e.emailId = "test@test.com";
//    e.region = "us";
//    dao.add(e);
//
//    PatientInfo email = dao.find();
//    System.out.println(email);
//  }
}
