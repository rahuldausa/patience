package com.healspace.clari.notifier;

public interface ClariSmsSender {

	public void sendSms(PatientInfo pi);
}
