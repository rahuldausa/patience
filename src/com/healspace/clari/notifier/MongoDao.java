package com.healspace.clari.notifier;

import java.net.UnknownHostException;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;
import com.mongodb.MongoOptions;
import com.mongodb.WriteResult;

public class MongoDao {

  private static final Logger log = Logger.getLogger(MongoDao.class);
  //private static DB db;
  
  /**
   *  it is for each instance of MongoDao
   */
  protected MongoClient mongoClient;

  /**
   * Name of the MongoDB collection Name
   */
  private String collectionName;
  String serverAddress;

  private static Map<String, DB> dbMap = new ConcurrentHashMap<String, DB>();

  private static Map<String, DBCollection> dbCollectionMap = new ConcurrentHashMap<String, DBCollection>();

  public MongoDao(String dbName, String collectionName) {
    this("127.0.0.1", dbName, collectionName);
  }
  
  public MongoDao(String serverAddress, String dbName, String collectionName) {
    this.serverAddress = serverAddress;
    this.collectionName = collectionName;
    try {
      // on constructor load initialize MongoDB and load collection
      DB db = initMongoDB(dbName);
      if (collectionName != null) {
        DBCollection collection = db.getCollection(collectionName);
        dbCollectionMap.put(collectionName, collection);
      }
    } catch (MongoException ex) {
      log.error("MongoException :" + ex.getMessage());
    }
  }
  
  /**
   * initMongoDB been called in constructor so every object creation this initialize MongoDB.
   */
  public DB initMongoDB(String dbName) throws MongoException {
    DB db = dbMap.get(serverAddress);
//    if (db != null) {
//      log.info("MongoDB already initialized");
//      return db;
//    }
    try {
      log.info("Connecting to Mongo DB..");
      
       // SocketTimeout: 30s, ConnectionTimeout 15s, ReconnectRetry: 20min
      mongoClient = new MongoClient(serverAddress);
      System.out.println("mongoClient:"+mongoClient);
      MongoOptions options = mongoClient.getMongoOptions();
      //options.setAutoConnectRetry(true);
      //options.setMaxAutoConnectRetryTime(1200000);
      options.setSocketTimeout(30000);
      options.setConnectTimeout(15000);
//      Mongo mongo = new Mongo("23.227.177.112");

      db = mongoClient.getDB(dbName);
      log.info("Connected to Mongo DB.");
      dbMap.put(serverAddress, db);
      return db;
    } catch (UnknownHostException ex) {
      log.error("MongoDB Connection Errro :" + ex.getMessage());
    }
    return db;
  }
  
/*  public void addIndex(){
    DBCollection collection = dbCollectionMap.get(collectionName);
    collection.createIndex(new BasicDBObject("i", 1));  // create index on "i", ascending
  }*/
  
  public void addIndex(DBObject key){
    DBCollection collection = dbCollectionMap.get(collectionName);
    collection.ensureIndex(key);  // create index on "i", ascending
  }
  
  public void upsert(DBObject q, DBObject u) {
    DBCollection collection = dbCollectionMap.get(collectionName);
    try{
    collection.update(q, u, true, false);
    } catch (Exception e) {
      if (e instanceof com.mongodb.MongoException) {
        System.err.println(e.getMessage());
      } else {
        e.printStackTrace();
      }
    }
  }
  
  public boolean insert(DBObject o) {
    DBCollection collection = dbCollectionMap.get(collectionName);
    System.out.println(o + "--"+collectionName +"--"+collection);
    WriteResult wr = collection.insert(o);
    System.out.println("wr:"+wr.toString());
    if (wr != null && wr.getError() != null) {
      return false;
    }
    return true;
  }
  
  public boolean insertMulti(DBObject... arr) {
    DBCollection collection = dbCollectionMap.get(collectionName);
    WriteResult wr =collection.insert(arr);
    System.out.println("wr:"+wr.toString());
    if (wr != null && wr.getError() != null) {
      return false;
    }
    return true;
  }

  public boolean update(DBObject q, DBObject u) {
    DBCollection collection = dbCollectionMap.get(collectionName);
    //WriteConcern wc = new WriteConcern();
    WriteResult wr =collection.update(q, u);
    System.out.println("wr:"+wr.toString());
    if (wr != null && wr.getError() != null) {
      return false;
    }
    return true;
  }

  /**
   * void method print fetched records from mongodb This method use the preloaded items (Collection) for fetching
   * records and print them on console.
   * @return
   */
  public Iterator<DBObject> findRecords(DBObject queryCondition, DBObject queryFields) {
    DBCollection collection = dbCollectionMap.get(collectionName);
    DBCursor cursor = collection.find(queryCondition, queryFields);
    return cursor.iterator();
  }
  
  public Iterator<DBObject> findRecords(DBObject queryCondition, DBObject queryFields,
      DBObject sortBy, int skip, int limit) {
    //DBCollection collection = dbCollectionMap.get(collectionName);
    //DBCursor cursor = collection.find(queryCondition, queryFields);
    
    return streamResults(queryCondition, queryFields, sortBy, skip, limit).iterator();
    //return cursor.iterator();
  }
  
  /**
   * To check whether any document exists for the given query
   * @param queryCondition
   * @param queryFields
   * @return
   * https://blog.serverdensity.com/checking-if-a-document-exists-mongodb-slow-findone-vs-find/
   */
  public Iterator<DBObject> findOneRecord(DBObject queryCondition, DBObject queryFields) {
    DBCollection collection = dbCollectionMap.get(collectionName);
    DBCursor cursor = collection.find(queryCondition, queryFields).limit(1);
    return cursor.limit(1).iterator();
  }
  
  public Iterator<DBObject> findRecord(DBObject queryCondition, DBObject queryFields) {
    DBCollection collection = dbCollectionMap.get(collectionName);
    DBCursor cursor = collection.find(queryCondition, queryFields).limit(1);
    return cursor.iterator();
  }
  
  public Iterator<DBObject> findByObjectId(DBObject objId) {
    DBCollection collection = dbCollectionMap.get(collectionName);
    DBCursor cursor = collection.find(objId);
    return cursor.iterator();
  }
  
  public boolean exists(DBObject objId) {
    DBCollection collection = dbCollectionMap.get(collectionName);
    DBCursor cursor = collection.find(objId);
    if (cursor.count() > 0) {
      return true;
    }
    return false;
  }
  
  public boolean exists(DBObject queryCondition, DBObject queryFields) {
    DBCollection collection = dbCollectionMap.get(collectionName);
    DBCursor cursor = collection.find(queryCondition, queryFields).limit(1);
    if (cursor.count() > 0) {
      return true;
    }
    return false;
  }

  public Iterator<DBObject> streamResults(DBObject queryCondition, DBObject queryFields) {
    // DBObject query = BasicDBObjectBuilder.start("_id", "5360bfc092eac1a91eb6d378").get();
    // DBObject sortBy = BasicDBObjectBuilder.start("$natural", 1).get();
    DBCollection collection = dbCollectionMap.get(collectionName);
    DBCursor cursor = null;
    if (queryFields != null) {
      cursor = collection.find(queryCondition, queryFields);
    } else {
      cursor = collection.find(queryCondition);
    }
    return cursor.iterator();
  }

  public DBCursor streamResults(DBObject queryCondition, DBObject queryFields,
      DBObject sortBy, int skip, int limit) {
    // DBObject query = BasicDBObjectBuilder.start("_id", "5360bfc092eac1a91eb6d378").get();
    // DBObject sortBy = BasicDBObjectBuilder.start("$natural", 1).get();
    DBCollection collection = dbCollectionMap.get(collectionName);

    DBCursor cursor = null;
    if (queryFields != null) {
      cursor = collection.find(queryCondition, queryFields);
    } else {
      cursor = collection.find(queryCondition);
    }
    if (sortBy != null) {
      cursor = cursor.sort(sortBy);// cursor.sort(new BasicDBObject("_id",-1));
    }
    if (limit != -1) {
      cursor = cursor.limit(limit);
    }
    if (skip > 0) {// skip!=-1 && skip!=0
      cursor = cursor.skip(skip);
    }
    return cursor;
  }
  
  public Iterator<DBObject> streamResultsIterator(DBObject queryCondition, DBObject queryFields, DBObject sortBy,
      int skip, int limit) {
    return streamResults(queryCondition, queryFields, sortBy, skip, limit).iterator();
  }
  
  public boolean delete(DBObject queryCondition) {
    DBCollection collection = dbCollectionMap.get(collectionName);
    WriteResult wr = collection.remove(queryCondition);
    System.out.println("wr:"+wr.toString());
    if (wr != null && wr.getError() != null) {
      return false;
    }
    return true;
  }
  
  public void shutdown(){
    if (mongoClient != null) {
      mongoClient.close();
    }
  }
}