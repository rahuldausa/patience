package com.healspace.clari.notifier;

import java.io.UnsupportedEncodingException;

public class DummySmsSenderImpl extends AbstractSmsSender implements ClariSmsSender {

	@Override
	public void sendSms(PatientInfo pi) {
		try {
			String requestUrl = createRequestUrl(pi);
			System.out.println(requestUrl);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

}
