package com.healspace.clari.notifier;

import java.util.LinkedList;
import java.util.List;

import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;

import com.healspace.clari.notifier.impl.PatientWaitingTimeCalculator;
import com.healspace.clari.notifier.impl.WaitingTimeNotifierManager;

public class PatientImpl {

	private static PatientImpl instance = new PatientImpl();

	public static PatientImpl getInstance() {
		return instance;
	}
	
	private ClariDao dao;
	
	private WaitingTimeNotifierManager waitingTimeNotifierManager;
	private WaitingTimeCalc waitingTimeCalc;
	
	public static int currentDoctorSeeingTokenNo = 0;
	//public static int currentProcessingTokenNo = 0;

	public  LinkedList<PatientInfo> incomingPatients = new LinkedList<PatientInfo>();
	public LinkedList<PatientInfo> donePatients = new LinkedList<PatientInfo>();
	
	private PatientImpl(){
		dao = new ClariDao();
		waitingTimeCalc = new MeanSDWaitingTimeCalc();
		waitingTimeNotifierManager = new WaitingTimeNotifierManager();
		waitingTimeNotifierManager.start();
		
		// TODO: we need to read existing records.
		//clearout();
        loadExistingRecords();
	}
	
	public void loadExistingRecords() {
		// Query q = dao.ds.createQuery(PatientInfo.class);
		List<PatientInfo> patients = dao.ds.find(PatientInfo.class).asList();
		for (PatientInfo pi : patients) {
			PatientInfo.currentPatientNumber.set(pi.getPatientNumber());
			PatientInfo.currentTokenNumber.set(pi.getTokenNumber());
			if (pi.isConsultationDone()) {
				donePatients.add(pi);
			} else {
				incomingPatients.add(pi);
			}
		}
		System.out.println("incomingPatients:" + incomingPatients);
		System.out.println("currentPatientNumber:"
				+ PatientInfo.currentPatientNumber + "--currentTokenNo:"
				+ PatientInfo.currentTokenNumber);
	}
	
	public void clearout(){
		dao.ds.delete(dao.ds.createQuery(PatientInfo.class));
	}

	public PatientInfo checkin(PatientInfo patient) {
		System.out.println("Patient no:"+patient.getPatientName() + "--have:"+patient.getTokenNumber());
		patient.expectedWaitingTime = calculateExpectedWaitingPeriod(patient)[0];
		patient.avgConsultationTime = calculateExpectedWaitingPeriod(patient)[1];

		System.out.println("expecting time:"+patient.expectedWaitingTime);
		incomingPatients.add(patient);
		dao.add(patient);
		System.out.println(incomingPatients);
		return patient;
	}

	PatientWaitingTimeCalculator waitingTimeCalculator = new PatientWaitingTimeCalculator();
	
	public PatientInfo doctorShow() {
		if (incomingPatients.size() > 0) {
			PatientInfo pi = incomingPatients.removeFirst();// shoudl try peek, or poll
			currentDoctorSeeingTokenNo = pi.tokenNumber;
			// doctorConsultation();
			
			// this should be above donePatients.add() method
			waitingTimeCalculator.nextPatient(pi, donePatients, dao);
			
			donePatients.add(pi);
			
			// update Record in DB.
			updateRecord(pi);
			return pi;
		} else {
			System.out.println("No patients available");
			return null;
		}
	}
	
	private void updateRecord(PatientInfo pi){
		Query query = dao.ds.createQuery(PatientInfo.class).field("patientNumber").equal(pi.patientNumber);
		UpdateOperations<PatientInfo> ops = dao.ds.createUpdateOperations(PatientInfo.class)
				.set("consultationDone", "true")
				.set("doctorCallTime", pi.getDoctorCallTime())
				.set("timeTakenInConsultaton", pi.getTimeTakenInConsultaton())
				.set("totalWaitingTime", pi.getTotalWaitingTime())
       ;					
		dao.ds.update(query, ops);
	}
	
	public long[] calculateExpectedWaitingPeriod(PatientInfo pi){
		System.out.println("token no:"+pi.getTokenNumber() + "--"+currentDoctorSeeingTokenNo);

		int personWaitingInQueue = pi.getTokenNumber() - currentDoctorSeeingTokenNo;
		System.out.println("personWaitingInQueue:"+personWaitingInQueue);
		//long avgConsultationTime = calculateAvgConsultationTime();
		Double d = waitingTimeCalc.calculateConsultationTime(donePatients)[1];
		System.out.println("waiting period:"+d);
		long avgConsultationTime = d.longValue();
		System.out.println("avgConsultationTime:"+avgConsultationTime);

		long waitingTime = personWaitingInQueue * avgConsultationTime;
		System.out.println("personWaitingInQueue:"+personWaitingInQueue+"--avgConsultationTime:"+avgConsultationTime+"--time:"+waitingTime);

		long[] vals = new long[2];
		vals[0]= waitingTime;
		vals[1] = avgConsultationTime;
		return vals;
	}
	
	private void doctorConsultation() {
		// doctor will see and take some time
	}
	
	public void stats(){
		
	}
	
	
}
