package com.healspace.clari.notifier;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.log4j.BasicConfigurator;

public class ClariSmsSenderImpl extends AbstractSmsSender  implements ClariSmsSender {

	public static void main(String[] args) {
		BasicConfigurator.configure();
		ClariSmsSender smsSender = new ClariSmsSenderImpl();
		//smsSender.sendSms("919963022661", "919908599937", "Hi Sandeep, your waiting time period is 10mins");
		//smsSender.sendSms("919908599937", "919963022661", "Hi Sandeep, your waiting time period is 10mins");
		
		PatientInfo pi = new PatientInfo();
		pi.patientName = "Sandeep";
		pi.mobileNumber="9963022661";
		pi.expectedWaitingTime= 180;
		smsSender.sendSms(pi);
	}
	
	public void sendSms(PatientInfo pi) {
		try {
			String requestUrl = createRequestUrl(pi);

			HttpClient client = new DefaultHttpClient();
			client.getParams().setParameter(
					CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
			HttpPost request = new HttpPost(requestUrl);
			HttpResponse resp = client.execute(request);
			System.out.println("response message:" + resp.getStatusLine());
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println(ex.getMessage());
		}
	}
}
