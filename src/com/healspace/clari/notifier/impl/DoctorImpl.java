package com.healspace.clari.notifier.impl;

import java.util.Date;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

public class DoctorImpl {

	private long lastPatientTime;
	private long currentPatientTime;
	private long timeTakenInConsultaton;
	// while reception
	private AtomicInteger incomingPatientNo = new AtomicInteger(0);

	// while doctor is calling for enter into doctor's room.
	private AtomicInteger clearningPatientNo = new AtomicInteger(0);

	public void nextPatient() {
		if (lastPatientTime != 0) {
//			timeTakenInConsultaton = System.currentTimeMillis()
//					- lastPatientTime;
			
			timeTakenInConsultaton = calcualteConsultationTime(System.currentTimeMillis(), lastPatientTime);
		} else {
			System.out
					.println("The first patient entering in consultation room");
		}
		
		if (lastPatientTime == 0) {
			lastPatientTime = System.currentTimeMillis();
		}
		
		clearningPatientNo.incrementAndGet();
		
		currentPatientTime = System.currentTimeMillis();
		
		System.out.println("Calling patient " + clearningPatientNo.get()
				+ " at:" + new Date(currentPatientTime) + "");
		
		doctorStartConsultation();
		
		System.out.println("Time taken in consultation for patient "
				+ clearningPatientNo.get() + " is:" + timeTakenInConsultaton
				+ ""); 
		lastPatientTime = currentPatientTime;
	}
	
	private Random rand = new Random(1000);
	
	public void doctorStartConsultation(){
		try {
			//Thread.sleep(rand.nextInt(5000));
			Thread.sleep(1000L);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public long calcualteConsultationTime(long end, long start){
		return end-start;
	}


	public static void main(String[] args) {
		DoctorImpl doctorConsult = new DoctorImpl();
		try {
			doctorConsult.nextPatient();
			Thread.sleep(1000L);
			doctorConsult.nextPatient();
			Thread.sleep(2000L);
			doctorConsult.nextPatient();
			Thread.sleep(2000L);
			doctorConsult.nextPatient();
			Thread.sleep(4000L);
			doctorConsult.nextPatient();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
