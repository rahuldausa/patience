package com.healspace.clari.notifier.impl;

import java.util.ArrayList;
import java.util.List;

public class ConsulatantTimePoints {

	List<ConsulatationTimePoint> points;

	public ConsulatantTimePoints() {
		points = new ArrayList<ConsulatationTimePoint>();
	}

	public void add(ConsulatationTimePoint e) {
		this.points.add(e);
	}

	@Override
	public String toString() {
		return "ConsulatantTimePoints [points=" + points + "]";
	}
	
	
}
