package com.healspace.clari.notifier.impl;

import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;

import com.healspace.clari.notifier.ClariDao;
import com.healspace.clari.notifier.Mean2SDWaitingTimeCalc;
import com.healspace.clari.notifier.PatientInfo;
import com.healspace.clari.notifier.WaitingTimeCalc;

public class PatientWaitingTimeCalculator {

//	private long lastPatientTime;
//	private long currentPatientTime;
//	private long timeTakenInConsultaton;
	// while reception
	//private AtomicInteger incomingPatientNo = new AtomicInteger(0);

	// while doctor is calling for enter into doctor's room.
	//private AtomicInteger clearningPatientNo = new AtomicInteger(0);
	
	private PatientInfo currentPatient;
	private PatientInfo lastPatient;
	
	private int minThreshold = 30;// 30 secs
	private int maxThreshold = 1200;// 600 seconds
	
	WaitingTimeCalc waitingTimgCalc;

	public PatientWaitingTimeCalculator() {
		waitingTimgCalc = new Mean2SDWaitingTimeCalc();
	}

	public void nextPatient(PatientInfo currentPatient, List<PatientInfo> donePatients, ClariDao dao) {
		if (lastPatient != null) {
			// return in seconds.
			lastPatient.timeTakenInConsultaton = calcualteConsultationTimeInSecs(
					System.currentTimeMillis(), lastPatient.doctorCallTime);
			
             avgOutOutliars(lastPatient, donePatients);
			
			// return in seconds.
			lastPatient.totalWaitingTime = waitingTimeInSecs(lastPatient);

			System.out.println("Time taken in consultation for patient "
					+ lastPatient.getTokenNumber() + " is:"
					+ lastPatient.timeTakenInConsultaton + "");
			updatePreviousPatientConsultationTime(lastPatient, dao);
		}
		System.out.println("Calling patient with token: " + currentPatient.getTokenNumber()
				+ " at:" + new Date(System.currentTimeMillis()) + "");
		
		currentPatient.doctorCallTime = System.currentTimeMillis();
		this.currentPatient = currentPatient;
		
		// making him last patient
		this.lastPatient = currentPatient;
	}
	
	public void avgOutOutliars(PatientInfo pi, List<PatientInfo> donePatients) {
		if (pi.timeTakenInConsultaton < minThreshold
				|| pi.timeTakenInConsultaton > maxThreshold) {
			// calculating the mean time in consultation
			// and setting it for all outliars.
			// resetting to mean value
			pi.timeTakenInConsultaton = ((Double) waitingTimgCalc
					.calculateConsultationTime(donePatients)[2]).longValue();
		}
	}
	
	private void updatePreviousPatientConsultationTime(PatientInfo pi, ClariDao dao){
		Query query = dao.ds.createQuery(PatientInfo.class).field("patientNumber").equal(pi.patientNumber);
		UpdateOperations<PatientInfo> ops = dao.ds.createUpdateOperations(PatientInfo.class)
				.set("consultationDone", "true")
				//.set("doctorCallTime", pi.getDoctorCallTime())
				.set("timeTakenInConsultaton", pi.getTimeTakenInConsultaton())
				.set("totalWaitingTime", pi.getTotalWaitingTime())
       ;					
		dao.ds.update(query, ops);
	}
	
//	public void test(){
//		if (lastPatientTime != 0) {
//			timeTakenInConsultaton = calcualteConsultationTime(System.currentTimeMillis(), lastPatientTime);
//		} else {
//			System.out
//					.println("The first patient entering in consultation room");
//		}
//		
//		if (lastPatientTime == 0) {
//			lastPatientTime = System.currentTimeMillis();
//		}
//		
//		clearningPatientNo.incrementAndGet();
//		
//		currentPatientTime = System.currentTimeMillis();
//		
//		System.out.println("Calling patient " + clearningPatientNo.get()
//				+ " at:" + new Date(currentPatientTime) + "");
//		
//		doctorStartConsultation();
//		
//		System.out.println("Time taken in consultation for patient "
//				+ clearningPatientNo.get() + " is:" + timeTakenInConsultaton
//				+ ""); 
//		lastPatientTime = currentPatientTime;
//	}
	
	private Random rand = new Random(1000);
	
	public void doctorStartConsultation(){
		try {
			//Thread.sleep(rand.nextInt(5000));
			Thread.sleep(1000L);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public long calcualteConsultationTimeInSecs(long end, long start){
		return TimeUnit.MILLISECONDS.toSeconds(end-start);
	}
	
	public long waitingTimeInSecs(PatientInfo pi){
		return TimeUnit.MILLISECONDS.toSeconds(pi.doctorCallTime - pi.receptionEntryTime);
	}

	public static void main(String[] args) {
		PatientWaitingTimeCalculator doctorConsult = new PatientWaitingTimeCalculator();
		PatientInfo pi = new PatientInfo();
//		try {
//			doctorConsult.nextPatient(pi);
//			Thread.sleep(1000L);
//			doctorConsult.nextPatient();
//			Thread.sleep(2000L);
//			doctorConsult.nextPatient();
//			Thread.sleep(2000L);
//			doctorConsult.nextPatient();
//			Thread.sleep(4000L);
//			doctorConsult.nextPatient();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}

	}
	
	 
}
