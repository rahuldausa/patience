package com.healspace.clari.notifier.impl;

import java.util.Date;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.healspace.clari.notifier.ClariSmsSender;
import com.healspace.clari.notifier.ClariSmsSenderImpl;
import com.healspace.clari.notifier.DummySmsSenderImpl;
import com.healspace.clari.notifier.PatientImpl;
import com.healspace.clari.notifier.PatientInfo;

public class WaitingTimeNotifierManager {

	private ScheduledExecutorService service;

	public WaitingTimeNotifierManager() {
		service = new ScheduledThreadPoolExecutor(1);
	}

	public void start() {
		service.scheduleAtFixedRate(new WaitingTimeNotifier(), 30, 30, TimeUnit.SECONDS);
	}

	public class WaitingTimeNotifier implements Runnable {

		@Override
		public void run() {
			System.out.println("WaitingTimeNotifier is running at:"+new Date());
			notifyPatient();
		}
		
		public void notifyPatient(){
			//ClariSmsSender smsSender = new DummySmsSenderImpl();
			ClariSmsSender smsSender = new ClariSmsSenderImpl();

			PatientImpl impl = PatientImpl.getInstance();
			
			int currentTokenNo = impl.currentDoctorSeeingTokenNo;
			
			// including first patient also.
			if (!(impl.incomingPatients.size() > 1)) {
				return;
			}
			
			for (PatientInfo pi : impl.incomingPatients) {
				System.out.println("pi:" + pi.getTokenNumber() + " --"
						+ currentTokenNo);
				// if his token number becoming less than current token and 5
				// more tokens
				if (pi.getTokenNumber() == 1) {
					System.out
							.println("Not sending sms as being first patient");
					continue;
				}
				if (pi.getTokenNumber() <= currentTokenNo + 5) {
					// send Notification, if we havent send previously sent
					// communiction.
					if (!pi.smsSentToPatitent) {
						smsSender.sendSms(pi);
						pi.smsSentToPatitent = true;
						pi.smsSentTime = System.currentTimeMillis();
					} else {
						System.out.println("Already sent sms to user");
					}
				}else{
					//System.out.println("patient token number is greater than currentToken+5");
				}
			}
		}
	}
	
	public static void main(String[] args) {

	}
}
