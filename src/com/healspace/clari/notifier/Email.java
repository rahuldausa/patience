package com.healspace.clari.notifier;

import java.io.Serializable;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Indexed;

@Entity("emails")
public class Email implements Serializable {

  @Id
  ObjectId id;

  @Indexed(unique = true)
  String emailId;

  @Indexed
  String region;

  @Indexed
  boolean active = true;

  @Indexed
  boolean unsubscribed = false;

  @Override
  public String toString() {
    return "Email [emailId=" + emailId + ", region=" + region + "]";
  }

}
