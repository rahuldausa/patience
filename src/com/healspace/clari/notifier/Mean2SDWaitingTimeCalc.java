package com.healspace.clari.notifier;

import java.util.List;

public class Mean2SDWaitingTimeCalc implements WaitingTimeCalc {

	@Override
	public double[] calculateConsultationTime(List<PatientInfo> donePatients) {
		return calculateMean2SDConsultationTime(donePatients);
	}

	private double[] calculateMean2SDConsultationTime(
			List<PatientInfo> donePatients) {
		long totalConsultantTime = 0L;
		double[] arr = new double[donePatients.size()];
		
		for (PatientInfo patientInfo : donePatients) {
			totalConsultantTime = totalConsultantTime+ patientInfo.getTimeTakenInConsultaton();
		}
		
		// mean
		long mean = 0L;
		if (donePatients.size() > 0) {
			mean = totalConsultantTime / donePatients.size();
		}
		
		//---- SD
		
		int cnt=0;
		for (PatientInfo patientInfo : donePatients) {
			arr[cnt] = Math.pow((patientInfo.getTimeTakenInConsultaton() -mean) , 2);
			cnt++;
		}
		
		double sumOfTheArray = 0L;
		for (double d : arr) {
			sumOfTheArray += d;
		}
		double _2deviation = 2*(Math.sqrt(sumOfTheArray/arr.length));

		double minval = 0d;
		double maxval = 0d;
		
		if (donePatients.size() > 0) {
			minval = mean - _2deviation;
			maxval = mean + _2deviation;
		}

		double[] res = new double[3];
		res[0] = minval;
		res[1] = maxval;
		res[2] = mean;
		return res;
	}
	
}