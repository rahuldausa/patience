package com.healspace.clari.notifier.servlet;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.management.timer.TimerMBean;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.healspace.clari.notifier.PatientImpl;
import com.healspace.clari.notifier.PatientInfo;

/**
 * Servlet implementation class CheckinServlet
 */
@WebServlet("/CheckinServlet")
public class CheckinServlet extends BaseServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public CheckinServlet() {
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String action = request.getParameter("action");
		if (action == null) {
			action = "beforecheckin";
		}
		if (action.equals("beforecheckin")) {
			beforeCheckin(request, response);
		} else if (action.equals("checkin")) {
			checkinPatient(request, response);
		}
	}
	
	public void beforeCheckin(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

	    getServletContext().getRequestDispatcher("/jsp/beforechecin.jsp").forward(request, response);

	}
	
	public void checkinPatient(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String patientName = request.getParameter("patientName");
		String mobileNumer  = request.getParameter("mobileNumber");
		
		PatientInfo pi = new PatientInfo();
		pi.patientName = patientName;
		pi.mobileNumber = mobileNumer;
		pi.receptionEntryTime = System.currentTimeMillis();
		
		PatientInfo resp = patientImpl.checkin(pi);
		
		System.out.println(resp);
		
		long expectedWaitingTime = resp.expectedWaitingTime;
		String mins = ""+TimeUnit.SECONDS.toMinutes(expectedWaitingTime)+"mins";
		
		String format= "Thank you <b>%s</b> for visiting SuperQuik Clinics. "
				+ "<br> You token number is <b>%s</b>"
				+ "<br> The current avg. consultation time is <b>%s</b> secs"
				+ "<br> You expected wait time is <b>%s</b>"
				+ "<br> We will send you a reminder sms before your turn.";
		
		String message = String.format(format, patientName, resp.getTokenNumber(), pi.avgConsultationTime, mins);
		
		request.setAttribute("patientName", resp.getPatientName());
		request.setAttribute("token", resp.getTokenNumber());
		
		// TODO: we need to tell expected turn time;
		request.setAttribute("expectedTurnTime",  mins);
		request.setAttribute("message",  message);
		
	    getServletContext().getRequestDispatcher("/jsp/afterchecin.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		checkinPatient(request, response);
	}

}
