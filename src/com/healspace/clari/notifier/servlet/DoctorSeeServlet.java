package com.healspace.clari.notifier.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.healspace.clari.notifier.PatientImpl;
import com.healspace.clari.notifier.PatientInfo;

/**
 * Servlet implementation class DoctorServlet
 */
@WebServlet("/DoctorServlet")
public class DoctorSeeServlet extends BaseServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public DoctorSeeServlet() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String action = request.getParameter("action");
		if (action == null) {
			//action = "docconsult";
			action="docconsult";
		}
		if (action.equals("beforedocconsult")) {
			beforedoctorConsultation(request, response);
		}
		else if (action.equals("docconsult")) {
			doctorConsultation(request, response);
			getServletContext().getRequestDispatcher("/jsp/doctorscreen.jsp")
			.forward(request, response);
		}
//		} else if (action.equals("checkin")) {
//			checkinPatient(request, response);
//		}
	}
	
	public void beforedoctorConsultation(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    getServletContext().getRequestDispatcher("/jsp/doctorscreen.jsp").forward(request, response);
	}
	
	public void doctorConsultation(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PatientInfo resp = patientImpl.doctorShow();
		if (resp != null) {
			System.out.println("starting consultaiton for patient no."
					+ resp.getPatientName() + " having token "
					+ resp.tokenNumber);

			System.out.println(resp);
			request.setAttribute("patientName", resp.getPatientName());
			request.setAttribute("token", resp.getTokenNumber());

			String format = "Consulting patient <b>%s</b> with token number <b>%s</b>.";
			String message = String.format(format, resp.getPatientName(), resp.getTokenNumber());
			request.setAttribute("message", message);
		} else {
			request.setAttribute("message", "No patients available");
		}

//		getServletContext().getRequestDispatcher("/jsp/doctorscreen.jsp")
//				.forward(request, response);
		
		//getServletContext().getRequestDispatcher("/DoctorServlet?action=docconsult").include(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doctorConsultation(request, response);
		
		getServletContext().getRequestDispatcher("/jsp/doctorscreen.jsp")
		.include(request, response);
	}

}
